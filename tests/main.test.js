import Promise from "bluebird";
import cloneDeep from "lodash/cloneDeep";
import includes from "lodash/includes";

import GitlabHelper from "../lib/GitlabHelper";
import ShellHelper from "../lib/ShellHelper";
import {
    create,
    validateProjectType,
    getRenameFilesMessage
} from "../lib/index";

const apiProjectName = `api-sample-test`;
const libProjectName = `lib-sample-test`;
const ShellHelperBak = cloneDeep(ShellHelper);
const ogWorkingDir = process.cwd();
jest.setTimeout(500000);

beforeAll(async () => {
    // Make sure project with name doesn't exist so we can create it
    await deleteProject(apiProjectName);
    await deleteProject(libProjectName);

    // Wait for deletion to propagate
    await Promise.delay(3000);
});

afterAll(async () => {
    await cleanup();
});

// NOTE:
// we're not really testing the cli commands, but the logic around it.
// We do test the integration with gitlab though.
test("mock create new projects", async () => {
    // Mock ShellHelper functions
    mockShellHelper();

    // ShellHelper.cd(installationDir);
    await create("node_sls_api", apiProjectName);

    // Wait between creating projects to avoid rate limit being hit
    await Promise.delay(1000);

    await create("node_lib", libProjectName);
    restoreShellHelper();
});

test("test validation of project types", async () => {
    let expectedError;
    try {
        validateProjectType("random_type", "api-random");
    } catch (e) {
        expectedError = e;
    }
    expect(expectedError).toBeDefined();
    expect(expectedError.message).toBe(
        "Your type is not one of the following types: node_sls_api, node_lib, react_app"
    );
});

test("test validation of project name", async () => {
    let expectedError;
    try {
        validateProjectType("node_lib", "random-random");
    } catch (e) {
        expectedError = e;
    }
    expect(expectedError).toBeDefined();
    expect(expectedError.message).toBe(
        "Your project didn't fit the convention of 'lib-<name>'"
    );
});

test("handle missing token", async () => {
    let expectedError;
    try {
        GitlabHelper.validateAccessToken(null);
    } catch (e) {
        expectedError = e;
    }

    expect(expectedError.message).toBe(
        "Missing auth token env var `GITLAB_TOKEN`"
    );
});

test("internals of ShellHelper", async () => {
    await ShellHelper.exec("echo 'hi'");
    ShellHelper.cd("./node_modules");

    let expectedError;
    try {
        await ShellHelper.exec("random-command");
    } catch (e) {
        expectedError = e;
    }
    expect(expectedError).toBeDefined();
    expect(includes(expectedError.message, "not found")).toBe(true);
});

test("setup existing project list", async () => {
    const resp = await GitlabHelper.setupExistingProjectList([
        apiProjectName,
        libProjectName
    ]);
    expect(resp).toBeInstanceOf(Array);
    expect(resp).toHaveLength(2);

    expect(resp[0].name).toBe(apiProjectName);
    expect(resp[0].reason).toBeUndefined();
    expect(resp[0].success).toBe(true);

    expect(resp[1].name).toBe(libProjectName);
    expect(resp[1].reason).toBeUndefined();
    expect(resp[1].success).toBe(true);
});

test("handle error setting up non existing project", async () => {
    const resp = await GitlabHelper.setupExistingProjectList(["lib-faky-fake"]);
    expect(resp).toBeInstanceOf(Array);
    expect(resp).toHaveLength(1);

    expect(resp[0].name).toBe("lib-faky-fake");
    expect(resp[0].reason).toBe("Error: Project doesn't exist.");
    expect(resp[0].success).toBe(false);
});

test("get all project names", async () => {
    await GitlabHelper.getAllProjectNames();
});

test("tryCatcherIgnoreIfConditionString throwing error", async () => {
    const fn = () => {
        throw new Error("not it");
    };

    let expectedError;
    try {
        await GitlabHelper.tryCatcherIgnoreIfConditionString(
            fn,
            {},
            "needs to be it"
        );
    } catch (e) {
        expectedError = e;
    }
    expect(expectedError).toBeDefined();
});

test("handleError with object description and url", async () => {
    const e = new Error("Yo");
    e.description = {};
    e.response = {};
    e.response.url = "http://yikes.com";

    let expectedE;
    try {
        GitlabHelper.handleError(e);
    } catch (caughtE) {
        expectedE = caughtE;
        console.log(caughtE);
    }
    expect(expectedE).toBeDefined();
    expect(expectedE.message).toBe("Error: Yo. {} [URL: http://yikes.com]");
});

test("getRenameFilesMessage", async () => {
    const filesTouched = [
        { file: "test.js", hasChanged: true },
        { file: "testB.js", hasChanged: true }
    ];

    // Did replace
    const { msg: msgA } = getRenameFilesMessage(filesTouched);
    expect(msgA).toBe("Replaced in 2 files: test.js, testB.js");

    // Did not replace
    const { msg: msgB } = getRenameFilesMessage(
        { file: "test.js", hasChanged: false },
        { file: "testB.js", hasChanged: false }
    );
    expect(msgB).toBe(
        "No files thouched while replacing. Something seems wrong."
    );
});

async function deleteProject(projectNameLocal) {
    const project = await GitlabHelper.getProjectWithName(projectNameLocal);
    if (!project) {
        return null;
    }
    await GitlabHelper.deleteProject(project.id);
    return null;
}

function mockShellHelper() {
    ShellHelper.exec = cmd => {
        console.log(`Mocking... ${cmd}`);
    };

    ShellHelper.cd = cmd => {
        console.log(`Mocking... ${cmd}`);
    };
}

function restoreShellHelper() {
    ShellHelper.exec = ShellHelperBak.exec;
    ShellHelper.cd = ShellHelperBak.cd;
}

async function cleanup() {
    // Make sure we're back at og workign dir
    ShellHelper.cd(ogWorkingDir);
    await deleteProject(apiProjectName);
    await deleteProject(libProjectName);
}
