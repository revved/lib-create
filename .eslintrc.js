module.exports = {
    extends: ["@revved/node"],
    rules: {
        "lodash/import-scope": "off",
        "lodash/prefer-lodash-typecheck": "off"
    }
};
