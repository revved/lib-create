# lib-create

`lib-create` is a tool for creating new projects from prebuilt samples and installing all necessary [base configurations.](https://gitlab.com/revved/lib-dev-configs)
There's also a command to update exisint project settings to best practice ones.

## Usage

```bash
create Options:
  -t --type [type]  Project Type
  -n --name [name]  Name of the Project
  -h, --help        output usage information
```

```bash
update-projects Options:
  -n --names [names]  Names of the projects to update separated by commas
  -h, --help          output usage information
```

Examples:

```bash
npx @revved/lib-create@latest create -t node_lib -n lib-example
npx @revved/lib-create@latest create -t node_sls_api -n api-test-it
npx @revved/lib-create@latest update-projects -n revved-website,api-base
```

## Project Types

#### node_sls_api

Serverless API
Naming must follow the convention "api-<name>."

The sample comes from [api-sample](https://gitlab.com/revved/api-sample)

#### node_lib

Node Library
Naming must follow the convention "lib-<name>".

The sample comes from [lib-sample](https://gitlab.com/revved/lib-sample)
