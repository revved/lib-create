# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.2.0](https://gitlab.com/revved/lib-create/compare/v2.1.2...v2.2.0) (2019-09-25)


### Bug Fixes

* fixed test ([8e3787e](https://gitlab.com/revved/lib-create/commit/8e3787e))
* initial merge request uses default branch instead of always master ([15fdb71](https://gitlab.com/revved/lib-create/commit/15fdb71))


### Features

* added react app ([4250436](https://gitlab.com/revved/lib-create/commit/4250436))



### [2.1.2](https://gitlab.com/revved/lib-create/compare/v2.1.1...v2.1.2) (2019-09-24)


### Bug Fixes

* web app projects also need develop branch ([bf9b42e](https://gitlab.com/revved/lib-create/commit/bf9b42e))



### [2.1.1](https://gitlab.com/revved/lib-create/compare/v2.1.0...v2.1.1) (2019-09-24)


### Bug Fixes

* do not delete sample projects running tests ([30bca69](https://gitlab.com/revved/lib-create/commit/30bca69))
* do not use api-sample and lib-sample as test names ([9596bc5](https://gitlab.com/revved/lib-create/commit/9596bc5))


### Tests

* fixed rename message file test ([7341bdb](https://gitlab.com/revved/lib-create/commit/7341bdb))



## [2.1.0](https://gitlab.com/revved/lib-create/compare/v2.0.4...v2.1.0) (2019-09-24)


### Features

* rename all occurances of og project name ([33e1d3f](https://gitlab.com/revved/lib-create/commit/33e1d3f))



### [2.0.4](https://gitlab.com/revved/lib-create/compare/v2.0.3...v2.0.4) (2019-08-06)


### Bug Fixes

* make shelljs silent by default again ([5469f86](https://gitlab.com/revved/lib-create/commit/5469f86))



### [2.0.3](https://gitlab.com/revved/lib-create/compare/v2.0.2...v2.0.3) (2019-08-06)


### Bug Fixes

* allow CI user to push to protected branches ([0b5e825](https://gitlab.com/revved/lib-create/commit/0b5e825))



### [2.0.2](https://gitlab.com/revved/lib-create/compare/v2.0.1...v2.0.2) (2019-08-06)


### Bug Fixes

* do not push to master, instead push to another branch and open MR ([4cb3fbd](https://gitlab.com/revved/lib-create/commit/4cb3fbd))



### [2.0.1](https://gitlab.com/revved/lib-create/compare/v2.0.0...v2.0.1) (2019-08-06)


### Bug Fixes

* fixing create command ([7b6319b](https://gitlab.com/revved/lib-create/commit/7b6319b))
* fixing push step ([fe12c8b](https://gitlab.com/revved/lib-create/commit/fe12c8b))
* trying to fix issues cloning ([00869d5](https://gitlab.com/revved/lib-create/commit/00869d5))



## [2.0.0](https://gitlab.com/revved/lib-create/compare/v1.2.0...v2.0.0) (2019-08-05)


### Bug Fixes

* use edit approvals over approvals method ([021ad39](https://gitlab.com/revved/lib-create/commit/021ad39))


### docs

* document changes ([2f5ccdf](https://gitlab.com/revved/lib-create/commit/2f5ccdf))


### Tests

* 100% coverage ([fc93bca](https://gitlab.com/revved/lib-create/commit/fc93bca))
* fixed some tests that were failing ([d09a930](https://gitlab.com/revved/lib-create/commit/d09a930))


### BREAKING CHANGES

* cli now has more than one command, so previous usage needs to include `create`



## [1.2.0](https://gitlab.com/revved/lib-create/compare/v1.1.8...v1.2.0) (2019-08-01)


### Bug Fixes

* account for linux error difference when command not found ([f891553](https://gitlab.com/revved/lib-create/commit/f891553))


### Features

* completed project creation with gitlab api ([749926f](https://gitlab.com/revved/lib-create/commit/749926f))
* create project using gitlab api ([6650f9f](https://gitlab.com/revved/lib-create/commit/6650f9f))


### Tests

* increasing coverage ([2cd33c6](https://gitlab.com/revved/lib-create/commit/2cd33c6))
* setup test for create ([80cec2e](https://gitlab.com/revved/lib-create/commit/80cec2e))



### 1.1.8 (2019-07-31)


### Bug Fixes

* Added bin prop ([09aa4d1](https://gitlab.com/revved/lib-create/commit/09aa4d1))
