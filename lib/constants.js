export const TYPES = {
    node_sls_api: {
        config: "node_sls_api",
        sourceProjectName: "api-sample",
        sourceGitPath: "api-sample.git",
        startsWithText: "api-"
    },
    node_lib: {
        config: "node_lib",
        sourceProjectName: "lib-sample",
        sourceGitPath: "lib-sample.git",
        startsWithText: "lib-"
    },
    react_app: {
        config: "react_app",
        sourceProjectName: "web-app-sample",
        sourceGitPath: "web-app-sample.git",
        startsWithText: "web-app-"
    },
    react_native_app: {
        config: "react_native_app",
        sourceProjectName: "web-app-sample",
        sourceGitPath: "web-app-sample.git",
        startsWithText: "ReactNativeApp"
    }
};
