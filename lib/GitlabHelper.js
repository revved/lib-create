import Promise from "bluebird";
import filter from "lodash/filter";
import includes from "lodash/includes";
import get from "lodash/get";
import map from "lodash/map";
import find from "lodash/find";
import sortBy from "lodash/sortBy";
import startsWith from "lodash/startsWith";
import isObject from "lodash/isObject";
import toLower from "lodash/toLower";
import debugLib from "debug";
import autoBind from "auto-bind";
import { Gitlab } from "gitlab";

const debug = debugLib("lib-create");
const REVVED_GROUP_ID = "5023096";
const SLACK_WEBHOOK_VAR_NAME = "SLACK_NOTIFICATIONS_WEBHOOK";
const DEFAULT_BOARD_NAME = "Development";
const CI_USER_ID = 3851972;

class GitlabHelper {
    constructor() {
        const token = process.env.GITLAB_TOKEN;
        this.validateAccessToken(token);

        this.client = new Gitlab({
            token
        });

        autoBind(this);
    }

    validateAccessToken(token) {
        if (!token) {
            throw new Error("Missing auth token env var `GITLAB_TOKEN`");
        }
    }

    async setupNewProject(projectName) {
        return this.tryCatcher(
            this.setupNewProjectDefaultErrorHandling,
            projectName
        );
    }

    async setupNewProjectDefaultErrorHandling(projectName) {
        // Create project
        const createdProject = await this.createProject(projectName);
        const projectId = createdProject.id;
        // And update options
        await this.setupCommonProjectOptions(projectName, projectId);
        return createdProject;
    }

    async setupExistingProject(projectName) {
        await this.tryCatcher(
            this.setupExistingProjectDefaultErrorHandling,
            projectName
        );
    }

    async setupExistingProjectGraceful(projectName) {
        const newDebugInst = debugLib(`lib-create:existing:${projectName}`);
        newDebugInst(`Updating project`);

        const resp = { name: projectName, success: true };
        try {
            await this.setupExistingProject(projectName);
            newDebugInst(`Updated project`);
        } catch (e) {
            resp.success = false;
            resp.reason = e.message;

            newDebugInst(`Failed to update project: ${e.message}`);
        }
        return resp;
    }

    async setupExistingProjectDefaultErrorHandling(projectName) {
        const retrievedProject = await this.getProjectWithName(projectName);
        if (!retrievedProject) {
            throw new Error("Project doesn't exist");
        }
        const projectId = retrievedProject.id;

        const params = this.getCreateProjectDefaultOptions(projectName);

        // Delete params we don't wanna update
        delete params.name;
        delete params.visibility;

        // UPdate project
        const updatedProject = await this.updateProject(
            projectName,
            projectId,
            params
        );
        // And update options
        await this.setupCommonProjectOptions(projectName, projectId);
        return updatedProject;
    }

    async setupExistingProjectList(projectNameList) {
        const opts = {
            concurrency: 1
        };
        // eslint-disable-next-line lodash/prefer-lodash-method
        const resp = await Promise.map(
            projectNameList,
            async projectName => {
                return this.setupExistingProjectGraceful(projectName);
            },
            opts
        );

        return resp;
    }

    async setupCommonProjectOptions(projectName, projectId) {
        await this.setupProjectMergeRequestOptions(projectId);
        await this.setupProjectServices(projectId);
        await this.setupProjectPushRules(projectId);
        await this.setupProjectProtectedBranches(projectName, projectId);
        await this.setupProjectIssueBoard(projectId);
        await this.setupProjectEnvs(projectName, projectId);
    }

    // Project

    async createProject(projectName) {
        debug("Creating project");

        const params = this.getCreateProjectDefaultOptions(projectName);
        params.name = projectName;
        const createdProject = await this.client.Projects.create(params);
        const projectId = createdProject.id;

        // Create default branches
        this.createDefaultBranches(projectName, projectId);

        debug(`Created project ${createdProject.id}`);

        return createdProject;
    }

    async updateProject(projectName, projectId, params) {
        this.createDefaultBranches(projectName, projectId);

        const debugCommon = `project ${projectId}`;
        debug(`Updating ${debugCommon}`);
        const createdProject = await this.client.Projects.edit(
            projectId,
            params
        );

        debug(`Updated ${debugCommon}`);

        return createdProject;
    }

    async createDefaultBranches(projectName, projectId) {
        // Make sure `master` is always a branch
        await this.createProjectBranchIgnoringIfAlreadyExists(
            projectId,
            "master"
        );

        if (this.needsDevelopBranch(projectName)) {
            // Make sure `develop`` exists as we'll set it as default
            await this.createProjectBranchIgnoringIfAlreadyExists(
                projectId,
                "develop"
            );

            await this.createProjectBranchIgnoringIfAlreadyExists(
                projectId,
                "production"
            );
        }
    }

    needsDevelopBranch(projectName) {
        const needsTo =
            startsWith(projectName, "api-") ||
            startsWith(projectName, "web-app-") ||
            startsWith(projectName, "cdn-") ||
            startsWith(projectName, "ReactNativeApp");
        return needsTo;
    }

    async getProjectWithName(projectName) {
        debug(`Searching for project with name ${projectName}`);

        const projects = await this.client.GroupProjects.all(REVVED_GROUP_ID, {
            include_subgroups: true
        });
        debug(`Got all group projects`);
        const foundProject = find(projects, ["name", projectName]);
        let searchResMsg = foundProject ? "Found" : "Couldn't find";
        searchResMsg = `${searchResMsg} project with name ${projectName}`;
        debug(searchResMsg);

        return foundProject;
    }

    async deleteProject(projectId) {
        const debugCommon = `project ${projectId}`;
        debug(`Deleting ${debugCommon}`);

        await this.client.Projects.remove(projectId);

        debug(`Deleted ${debugCommon}`);
    }

    async getAllProjectNames() {
        const projects = await this.client.GroupProjects.all(REVVED_GROUP_ID);
        const names = map(projects, "name");
        return names;
    }

    // Merge requests

    async createMergeRequest(projectId, sourceBranch, targetBranch, title) {
        const resp = await this.client.MergeRequests.create(
            projectId,
            sourceBranch,
            targetBranch,
            title
        );
    }

    // Project merge options

    async setupProjectMergeRequestOptions(projectId) {
        const debugCommon = `up merge approvals for project ${projectId}`;
        debug(`Setting ${debugCommon}`);

        const params = {
            approvals_before_merge: 1,
            reset_approvals_on_push: true,
            disable_overriding_approvers_per_merge_request: true,
            merge_requests_author_approval: true
        };
        await this.client.MergeRequests.editApprovals(projectId, params);

        debug(`Set ${debugCommon}`);
    }

    // Project services

    async setupProjectServices(projectId) {
        const debugCommon = `up services for project ${projectId}`;
        debug(`Setting ${debugCommon}`);

        const webhook = await this.getSlackWebhookFromGitlabVar();
        const params = {
            username: "Revved bot",
            webhook,
            notify_only_broken_pipelines: false,
            notify_only_default_branch: false,
            deployment_channel: "#dev-deployments"
        };
        const serviceName = "slack";
        await this.client.Services.edit(projectId, serviceName, params);

        debug(`Set ${debugCommon}`);
    }

    // Project push rules

    async setupProjectPushRules(projectId) {
        const debugCommon = `up push rules for project ${projectId}`;
        debug(`Setting ${debugCommon}`);

        // Remove existing push if needed
        await this.tryCatcherIgnoreIfNotFound(
            this.deleteProjectPushRules,
            projectId
        );
        // throw new Error("Fuck");

        const params = {
            prevent_secrets: true,
            branch_name_regex:
                "^(develop|master)$|(feat/|bug/|fix/|chore/|release/)[a-z0-9-]+$" //eslint-disable-line
        };
        await this.client.PushRules.create(projectId, params);

        debug(`Set ${debugCommon}`);
    }

    async deleteProjectPushRules(projectId) {
        await this.client.PushRules.remove(projectId);
    }

    // Project protected branches

    async setupProjectProtectedBranches(projectName, projectId) {
        await this.setupProjectProtectedBranchIgnoringIfAlreadyExists(
            projectId,
            "master"
        );

        if (this.needsDevelopBranch(projectName)) {
            await this.setupProjectProtectedBranchIgnoringIfAlreadyExists(
                projectId,
                "develop"
            );
        }
    }

    async setupProjectProtectedBranchIgnoringIfAlreadyExists(
        projectId,
        branchName
    ) {
        return this.tryCatcherIgnoreIfAlreadyExists(
            this.setupProjectProtectedBranch,
            projectId,
            branchName
        );
    }

    async setupProjectProtectedBranch(projectId, branchName) {
        // Unprotect branch before protecting to handle changes
        await this.unprotectBranchIgnoringIfNotFound(projectId, branchName);

        // Nor protect it
        await this.protectBranch(projectId, branchName);
    }

    async protectBranch(projectId, branchName) {
        const debugCommon = `up protected branch ${branchName} for project ${projectId}`;
        debug(`Setting ${debugCommon}`);

        const noAccess = 0;
        const maintainerAccess = 40;
        const params = {
            name: branchName,
            allowed_to_push: [
                {
                    access_level: noAccess
                },
                {
                    user_id: CI_USER_ID
                }
            ],
            allowed_to_merge: [
                {
                    access_level: maintainerAccess
                }
            ]
        };

        await this.client.ProtectedBranches.protect(
            projectId,
            branchName,
            params
        );

        debug(`Set ${debugCommon}`);
    }

    async unprotectBranch(projectId, branchName) {
        const debugCommon = `branch ${branchName} for project ${projectId}`;
        debug(`Unprotecting ${debugCommon}`);
        await this.client.ProtectedBranches.unprotect(projectId, branchName);
        debug(`Unproteced ${debugCommon}`);
    }

    async unprotectBranchIgnoringIfNotFound(projectId, branchName) {
        await this.tryCatcherIgnoreIfNotFound(
            this.unprotectBranch,
            projectId,
            branchName
        );
    }

    async createProjectBranchIgnoringIfAlreadyExists(projectId, branchName) {
        return this.tryCatcherIgnoreIfAlreadyExists(
            this.createProjectBranch,
            projectId,
            branchName
        );
    }

    async createProjectBranch(projectId, branchName) {
        await this.client.Branches.create(projectId, branchName, "master");
    }

    // Project issue boards

    async setupProjectIssueBoard(projectId) {
        const debugCommon = `up issue board for project ${projectId}`;
        debug(`Setting ${debugCommon}`);

        const board = await this.createProjectIssueBoard(
            projectId,
            DEFAULT_BOARD_NAME
        );

        const labelsForBoard = await this.getLabelsForIssueBoard(projectId);
        await this.createAllProjectIssueBoardLists(
            projectId,
            board.id,
            labelsForBoard
        );

        // Delete the old board of the same name if needed
        await this.deleteOldProjectIssueBoard(
            projectId,
            DEFAULT_BOARD_NAME,
            board.id
        );

        debug(`Set ${debugCommon}`);
    }

    async createProjectIssueBoard(projectId, boardName) {
        const debugCommon = `issue board for project ${projectId}`;
        debug(`Creating ${debugCommon}`);

        const board = await this.client.ProjectIssueBoards.create(
            projectId,
            boardName
        );

        debug(`Created ${debugCommon}`);

        return board;
    }

    async deleteOldProjectIssueBoard(projectId, boardName, newBoardId) {
        const debugCommon = `issue board ${boardName} for project ${projectId}`;
        debug(`Deleting ${debugCommon}`);

        const boards = await this.client.ProjectIssueBoards.all(projectId);
        const neededBoard = find(boards, board => {
            return board.name === boardName && board.id !== newBoardId;
        });
        if (neededBoard) {
            await this.client.ProjectIssueBoards.remove(
                projectId,
                neededBoard.id
            );
            debug(`Deleted ${debugCommon}`);
        }
    }

    async createProjectIssueBoardList(projectId, boardId, label) {
        const debugCommon = `\`${label.name}\` issue board list for issue board ${boardId} for project ${projectId}`;
        debug(`Creatting ${debugCommon}`);

        await this.client.ProjectIssueBoards.createList(
            projectId,
            boardId,
            label.id
        );

        debug(`Created ${debugCommon}`);
    }

    async createAllProjectIssueBoardLists(projectId, boardId, labels) {
        // eslint-disable-next-line lodash/prefer-lodash-method
        // We have to use a series because gitlab will sort based on creation time
        await Promise.mapSeries(labels, label => {
            return this.createProjectIssueBoardList(projectId, boardId, label);
        });
    }

    async getLabelsForIssueBoard(projectId) {
        const debugCommon = `labels for project ${projectId}`;
        debug(`Getting ${debugCommon}`);

        const supportedLabels = [
            "status::Doing",
            "status::QA",
            "status::Won't Do"
        ];

        const labels = await this.client.Labels.all(projectId);

        const labelsForIssueBoard = filter(labels, label => {
            const isWorthy = includes(supportedLabels, label.name);
            return isWorthy;
        });

        debug(`Got ${debugCommon}`);

        const sortedLabelsForIssueBoard = sortBy(labelsForIssueBoard, label => {
            return supportedLabels.indexOf(label.name);
        });

        return sortedLabelsForIssueBoard;
    }

    // Project environments

    async setupProjectEnvs(projectName, projectId) {
        const debugCommon = `envs for project ${projectId}`;
        debug(`Setting ${debugCommon}`);

        const envParams = [
            {
                name: "Production",
                tier: "production"
            },
            {
                name: "Development",
                tier: "development"
            }
        ];

        if (startsWith(projectName, "api-")) {
            envParams.push(
                ...[
                    {
                        name: "Production Sls",
                        tier: "production"
                    },
                    {
                        name: "Development Sls",
                        tier: "development"
                    }
                ]
            );
        }

        await Promise.mapSeries(envParams, async params => {
            await this.createProjectEnvIgnoreIfAlreadyExists(projectId, params);
        });

        debug(`Set ${debugCommon}`);
    }

    async createProjectEnv(projectId, params) {
        try {
            await this.client.Environments.create(projectId, params);
        } catch (e) {
            throw e;
        }
    }

    async createProjectEnvIgnoreIfAlreadyExists(projectId, params) {
        const conditionString = "has already been taken";
        return this.tryCatcherIgnoreIfConditionString(
            conditionString,
            this.createProjectEnv,
            projectId,
            params
        );
    }

    // Secrets

    async getSlackWebhookFromGitlabVar() {
        debug(`Getting slack webhook url from gitlab secrets`);

        const resp = await this.client.GroupVariables.show(
            REVVED_GROUP_ID,
            SLACK_WEBHOOK_VAR_NAME
        );

        debug(`Got slack webhook url from gitlab secrets`);
        return resp.value;
    }

    // Helpers

    async tryCatcher(fn, ...params) {
        try {
            const resp = await fn(...params);
            return resp;
        } catch (e) {
            return this.handleError(e);
        }
    }

    async tryCatcherIgnoreIfAlreadyExists(fn, ...params) {
        const conditionString = "already exists";
        return this.tryCatcherIgnoreIfConditionString(
            conditionString,
            fn,
            ...params
        );
    }

    async tryCatcherIgnoreIfNotFound(fn, ...params) {
        const conditionString = "Not Found";
        return this.tryCatcherIgnoreIfConditionString(
            conditionString,
            fn,
            ...params
        );
    }

    async tryCatcherIgnoreIfConditionString(conditionString, fn, ...params) {
        try {
            const resp = await fn(...params);
            return resp;
        } catch (e) {
            let strToCheck = get(e, "description");
            if (isObject(strToCheck)) {
                strToCheck = JSON.stringify(strToCheck);
            }
            if (includes(toLower(strToCheck), toLower(conditionString))) {
                return null;
            }
            return this.handleError(e);
        }
    }

    handleError(e) {
        let description = get(e, "description");
        // For some reason gitlab can return `description` as an object
        if (isObject(description)) {
            description = JSON.stringify(description);
        }
        const url = get(e, "response.url", "");
        let contextStr = "";
        if (description) {
            contextStr += description;
        }
        if (url) {
            contextStr += ` [URL: ${url}]`;
        }
        contextStr = contextStr.length > 0 ? ` ${contextStr}` : "";
        const errMessage = `Error: ${e.message}.${contextStr}`;
        throw new Error(errMessage);
    }

    getCreateProjectDefaultOptions(projectName) {
        const defaultBranch = this.getDefaultBranchForProject(projectName);

        return {
            default_branch: defaultBranch,
            namespace_id: REVVED_GROUP_ID,
            visibility: "private",
            issues_access_level: "enabled",
            repository_access_level: "enabled",
            merge_requests_access_level: "enabled",
            wiki_access_level: "enabled",
            builds_access_level: "enabled",
            snippets_access_level: "enabled",
            shared_runners_enabled: true,
            lfs_enabled: true,
            auto_cancel_pending_pipelines: "enabled",
            only_allow_merge_if_pipeline_succeeds: true,
            approvals_before_merge: 1,
            request_access_enabled: false,
            only_allow_merge_if_all_discussions_are_resolved: true,
            auto_devops_enabled: false,
            merge_trains_enabled: true
        };
    }

    getDefaultBranchForProject(projectName) {
        let defaultBranch = "master";
        if (this.needsDevelopBranch(projectName)) {
            defaultBranch = "develop";
        }
        return defaultBranch;
    }
}

export default new GitlabHelper();
