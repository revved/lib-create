//* eslint-disable */

import shell from "shelljs";
import Promise from "bluebird";

class ShellHelper {
    async exec(cmd, opts = { silent: true }) {
        // eslint-disable-next-line promise/avoid-new
        return new Promise((resolve, reject) => {
            shell.exec(cmd, opts, (code, stdout, stderr) => {
                if (code !== 0) return reject(new Error(stderr));
                return resolve(stdout);
            });
        });
    }

    cd(dir) {
        shell.cd(dir);
    }
}

export default new ShellHelper();
