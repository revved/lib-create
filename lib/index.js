import { strict as assert } from "assert";
import startsWith from "lodash/startsWith";
import has from "lodash/has";
import keys from "lodash/keys";
import filter from "lodash/filter";
import join from "lodash/join";
import map from "lodash/map";
import Listr from "listr";
import chalk from "chalk";
import replace from "replace-in-file";

import { TYPES } from "./constants";
import GitlabHelper from "./GitlabHelper";
import ShellHelper from "./ShellHelper";

export async function create(type, name) {
    validateProjectType(type, name);
    const { startsWithText } = TYPES[type];
    const { sourceGitPath, sourceProjectName } = TYPES[type];
    const { config } = TYPES[type];

    const tasks = new Listr([
        getCloneAndNameTask(
            name,
            type,
            startsWithText,
            sourceGitPath,
            sourceProjectName
        ),
        getDevEnvironmentTask(config),
        getSetupGitTask(name, type)
    ]);

    await tasks.run();
}

function getCloneAndNameTask(
    name,
    type,
    startsWithText,
    sourceGitPath,
    sourceProjectName
) {
    return {
        title: `Clone ${startsWithText}sample -> ${name}`,
        task: () => {
            return new Listr([
                {
                    title: `Validate naming convention `,
                    task: () => {
                        validateProjectType(type, name);
                    }
                },
                {
                    title: `Cloning repository from ${sourceGitPath} `,
                    task: async () => {
                        await ShellHelper.exec(
                            `git clone git@gitlab.com:revved/${sourceGitPath} ./${name}`
                        );
                    }
                },
                {
                    title: `Changing current directory to ./${name}`,
                    task: () => ShellHelper.cd(`./${name}`)
                },
                {
                    title: `Replace all occurances of ${sourceProjectName}`,
                    task: async () => renameFiles(sourceProjectName, name)
                },
                {
                    title: "Remove old origin",
                    task: async () => {
                        await ShellHelper.exec("git remote rm origin");
                    }
                }
            ]);
        }
    };
}

function getDevEnvironmentTask(config) {
    return {
        title: "Set up dev environment",
        task: () => {
            return new Listr([
                {
                    title: `Installing Npm`,
                    task: async () => {
                        await ShellHelper.exec("npm ci");
                    }
                },
                {
                    title: `Setup ${config} configs `,
                    task: async () => {
                        await ShellHelper.exec(
                            `npx @revved/lib-dev-configs@latest setup-project ${config}` // eslint-disable-line no-secrets/no-secrets
                        );
                    }
                }
            ]);
        }
    };
}

function getSetupGitTask(name, type) {
    const initialBranchName = "chore/initial-push";
    let projectId;
    return {
        title: `Setup Git in ${name}`,
        task: () => {
            return new Listr([
                {
                    title: "Create project in gitlab",
                    task: async () => {
                        const createdProject = await GitlabHelper.setupNewProject(
                            name
                        );
                        projectId = createdProject.id;
                    }
                },
                {
                    title: "Add remote",
                    task: async () => {
                        await ShellHelper.exec(
                            `git remote add origin git@gitlab.com:revved/${name}.git`
                        );
                    }
                },
                {
                    title: "Add all new files",
                    task: async () => {
                        await ShellHelper.exec("git add .");
                    }
                },
                {
                    title: "Commit without Husky verification",
                    task: async () => {
                        await ShellHelper.exec(
                            `git commit --no-verify -m 'new ${type}'`
                        );
                    }
                },
                {
                    title: "Push without Husky verification",
                    task: async () => {
                        await ShellHelper.exec(
                            `git push -u -f --no-verify origin HEAD:${initialBranchName}` // eslint-disable-line
                        );
                    }
                },
                {
                    title: "Create merge request",
                    task: async () => {
                        const defaultBranch = GitlabHelper.getDefaultBranchForProject(
                            name
                        );
                        await GitlabHelper.createMergeRequest(
                            projectId,
                            initialBranchName,
                            defaultBranch,
                            "Initial setup"
                        );
                    }
                }
            ]);
        }
    };
}

export function validateProjectType(type, name) {
    assert.ok(name, "Please provide the name of the project with -n");
    assert.ok(type, "Please provide the type of the project with -t");
    if (!has(TYPES, type)) {
        throw new Error(
            `Your type is not one of the following types: ${typesList()}`
        );
    }

    if (
        TYPES[type].startsWithText &&
        !startsWith(name, TYPES[type].startsWithText)
    ) {
        throw new Error(
            `Your project didn't fit the convention of '${TYPES[type].startsWithText}<name>'`
        );
    }
}

function typesList() {
    const types = keys(TYPES);
    return types.join(", ");
}

export async function renameFiles(from, to) {
    console.log(chalk.blue(`Replacing in all files: ${from} -> ${to}`));

    const fromRegex = new RegExp(from, "g");
    const options = {
        files: "**/*",
        from: fromRegex,
        to,
        ignore: ["lib/index.js", "node_modules/**/*"],
        dry: getDryRunFlag(process.env.NODE_ENV)
    };

    const renameResults = await replace(options);
    const { msg, msgColor } = getRenameFilesMessage(renameResults);

    // Log a message showing home many files were touched.
    console.log(msgColor(msg));
}

export function getDryRunFlag(nodeEnv) {
    return nodeEnv === "test";
}

export function getRenameFilesMessage(renameResults) {
    const filesTouched = filter(renameResults, "hasChanged");
    const msgColor = filesTouched.length > 0 ? chalk.blue : chalk.red;
    const filesTouchedStrList = join(map(filesTouched, "file"), ", ");

    const msg =
        filesTouched.length > 0
            ? `Replaced in ${filesTouched.length} files: ${filesTouchedStrList}`
            : `No files thouched while replacing. Something seems wrong.`;

    // Log a message showing home many files were touched.
    return { msg, msgColor };
}
