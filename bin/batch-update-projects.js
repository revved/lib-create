#!/usr/bin/env node

import GitlabHelper from "../lib/GitlabHelper";

async function go() {
    const allProjectNames = await GitlabHelper.getAllProjectNames();
    console.log("allProjectNames", allProjectNames);
}

go();
