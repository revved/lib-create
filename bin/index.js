#!/usr/bin/env node

import program from "commander";
import chalk from "chalk";
import split from "lodash/split";
import uniq from "lodash/uniq";
import isString from "lodash/isString";
import groupBy from "lodash/groupBy";
import map from "lodash/map";
import join from "lodash/join";

import { create } from "../lib/index";
import GitlabHelper from "../lib/GitlabHelper";

program
    .description("Create new project")
    .command("create")
    .option("-t --type [type]", "Project Type")
    .option("-n --name [name]", "Name of the Project")
    .action(async cmd => {
        const { name, type } = cmd;

        await create(type, name);
    });

program
    .description("Update existing projects")
    .command("update-projects")
    .option(
        "-n --names [names]",
        "Names of the projects to update separated by commas"
    )
    .action(async cmd => {
        let projectNameList = isString(cmd.names)
            ? uniq(split(cmd.names, ","))
            : null;
        if (!projectNameList) {
            projectNameList = await GitlabHelper.getAllProjectNames();
        }

        const resp = await GitlabHelper.setupExistingProjectList(
            projectNameList
        );
        printBulkRespMessage(resp);
    });

// CLI extra setup

program.on("--help", () => {
    console.log("");
    console.log("Examples:");
    console.log("  $ create -t node_lib -n lib-example");
    console.log("  $ create -t node_sls_api -n api-example");
    console.log("  $ update-projects -n revved-website,api-base");
});

program.parse(process.argv);

if (process.argv.length <= 2) {
    if (!program.args.length) {
        program.help();
    }
}

// Handle unknown commands
program.on("command:*", () => {
    let errMsg = `Invalid command: ${program.args.join(" ")}\n`;
    errMsg += "See --help for a list of available commands.";
    const error = new Error(errMsg);
    handleError(error);
});

process.on("uncaughtException", err => {
    handleError(err);
});

// Helpers

function printBulkRespMessage(resp) {
    const sortedBySuccess = groupBy(resp, "success");
    const failedNamesStr = join(map(sortedBySuccess.false, "name"), ",");
    const succededNamesStr = join(map(sortedBySuccess.true, "name"), ",");
    if (failedNamesStr.length > 0) {
        printError(`Failed to update projects: ${failedNamesStr}`);
    }

    if (succededNamesStr.length > 0) {
        printSuccess(`Updated projects: ${succededNamesStr}`);
    }
}

// function exit(errorOrResp) {
//     if (errorOrResp && errorOrResp instanceof Error) {
//         console.log(chalk.red(`Error: ${errorOrResp.message}`));
//         process.exit(1);
//     }
//     process.exit();
// }

function handleError(e) {
    printError(e.message);
    process.exit(e);
}

function printError(msg) {
    console.log(chalk.red(msg));
}

function printSuccess(msg) {
    console.log(chalk.green(msg));
}
